package main;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import scenemanager.SceneController;
import scenemanager.SceneManager;

public class Menu extends SceneController {

    @FXML private Button playButton;
    @FXML private Button ultraViolenceButton;
    @FXML private Button exitButton;

    public Menu() {
    }

    @Override
    public void load(SceneManager sceneManager, String configFilePath, Object... args){
        super.load(sceneManager, configFilePath);
        ultraViolenceButton.setOnMouseClicked(mouseEvent -> {
            this.sceneManager.next(true);
        });
        exitButton.setOnMouseClicked(mouseEvent -> {
            System.exit(1);
        });
    }

    public void onMouseClicked(MouseEvent mouseEvent) {
        this.sceneManager.next(false);
    }
}
