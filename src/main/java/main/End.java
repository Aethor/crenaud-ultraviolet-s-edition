package main;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import scenemanager.SceneController;
import scenemanager.SceneManager;

public class End extends SceneController {

    @FXML private Button exitButton;

    public End(){}

    @Override
    public void load(SceneManager sceneManager, String configFilePath, Object... args){
        super.load(sceneManager, configFilePath);

        exitButton.setOnMouseClicked(mouseEvent -> {
            System.exit(1);
        });
    }
}
