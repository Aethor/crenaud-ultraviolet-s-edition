package main;

import agents.mexicans.MexicanManifestation;
import camera.MovableJfxCamera;
import displayable.Displayable;
import environnement.Environnement;
import environnement.city.Building;
import environnement.city.HittableBuilding;
import environnement.city.NeutralBuilding;
import environnement.sombreragram.SombreraPost;
import io.janusproject.Boot;
import io.sarl.bootstrap.SRE;
import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import quadtree.Quadtree;
import scenemanager.SceneController;
import scenemanager.SceneManager;

import java.util.ArrayList;
import java.util.LinkedList;

public class Game extends SceneController {

    @FXML private Canvas canvas;
    @FXML private VBox sombreraGramBox;
    @FXML private ScrollPane sombreraScroll;

    @FXML private Button tacosButton;
    @FXML private Button campButton;
    @FXML private Label moneyLabel;
    @FXML private Label infoLabel;

    @FXML private Button debugButton;
    @FXML private Label civilianLabel;
    @FXML private Label ultraVioletsLabel;
    @FXML private Label mercenaryLabel;

    private MovableJfxCamera camera;

    private Environnement environnement = null;

    public static boolean isDebug = true;

    private Point2D dragStart;

    public Game(){
    }

    @Override
    public void load(SceneManager sceneManager, String configFilePath, Object... args){

        super.load(sceneManager, configFilePath);

        if ((boolean) args[0]){
            System.out.println("[warning] Ultra Violence Mode activated");
        }

        canvas.setOnMousePressed(mouseEvent -> {
            dragStart = new Point2D(mouseEvent.getX(), mouseEvent.getY());

            if (environnement == null)
                return;

            Point2D gamePosition = camera.fromScreenToWorldSpace(new Point2D(mouseEvent.getX(), mouseEvent.getY()));
            Building building = environnement.getClosestBuilding(gamePosition, 100);
            environnement.selectBuilding(building);
        });
        canvas.setOnMouseDragged(mouseEvent -> {
            this.camera.setPosition(this.camera.getPosition().add((new Point2D(mouseEvent.getX(), mouseEvent.getY())).subtract(dragStart).multiply(-1 * (1/camera.getZoom()))));
            dragStart = new Point2D(mouseEvent.getX(), mouseEvent.getY());
        });
        canvas.setOnScroll(scrollEvent -> {
            if (scrollEvent.getDeltaY() > 0){
                this.camera.zoom(1.5);
            } else {
                this.camera.zoom(0.75);
            }
        });

        tacosButton.setOnMouseClicked(mouseEvent -> {
            if (environnement == null)
                return;
            Building selectedBuilding = environnement.getSelectedBuilding();
            if (selectedBuilding == null)
                return;
            if (selectedBuilding instanceof NeutralBuilding)
                environnement.convertToTacos((NeutralBuilding) selectedBuilding);
        });

        campButton.setOnMouseClicked(mouseEvent -> {
            if (environnement == null)
                return;
            Building selectedBuilding = environnement.getSelectedBuilding();
            if (selectedBuilding == null)
                return;
            if (selectedBuilding instanceof NeutralBuilding)
                environnement.convertToCamp((NeutralBuilding) selectedBuilding);
        });

        debugButton.setOnMouseClicked(mouseEvent -> {
            Config.isDebug = !Config.isDebug;
        });

        this.canvas.requestFocus();
        this.camera = new MovableJfxCamera(this.canvas);
        try {
            SRE.getBootstrap().startAgent(
                    Environnement.class,
                    this,
                    1,
                    this.canvas.getWidth(),
                    this.canvas.getHeight(),
                    (boolean) args[0] //isUltraViolence
            );
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Could not start environnement... exitting.");
            System.exit(1);
        }

        GraphicsContext gc = canvas.getGraphicsContext2D();
        while (environnement == null);
        this.camera.setZoom(0.4);
        this.camera.centerOn(environnement.getCity().getCenter());
        sombreraScroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        new AnimationTimer() {
            @Override
            public void handle(long l) {
                gc.setStroke(Color.BLACK);

                //display mexicans
                ArrayList<Displayable<MovableJfxCamera>> toDisplay = new ArrayList<>();
                toDisplay.addAll(environnement.getMexicanManifestations());
                toDisplay.addAll(environnement.getCity().buildings);
                camera.display(new ArrayList<>(toDisplay));

                //sombreragram display
                sombreraScroll.setVvalue(sombreraScroll.vmaxProperty().doubleValue());
                for (SombreraPost post : environnement.getSombreraGram().getLastPosts()){
                    Button visualPost = new Button(post.toString());
                    visualPost.setDisable(true);
                    visualPost.setWrapText(true);
                    visualPost.setMinHeight(80);
                    visualPost.setMinWidth(200);
                    visualPost.setTextAlignment(TextAlignment.LEFT);
                    sombreraGramBox.getChildren().add(visualPost);
                }

                //selected building hover display
                Building selectedBuilding = environnement.getSelectedBuilding();
                if (selectedBuilding != null){
                    Rectangle2D collider = selectedBuilding.getCollider();
                    gc.setStroke(Color.GREEN);
                    gc.strokeRect(
                             collider.getMinX() - 5 - camera.getPosition().getX(),
                            collider.getMinY() - 5 - camera.getPosition().getY(),
                            collider.getWidth() + 10,
                            collider.getHeight() + 10
                    );
                }

                //player info display
                moneyLabel.setText("    Money : " + environnement.getMoney());
                if (selectedBuilding != null){
                    String display = "building : " + selectedBuilding.getTypeAsString() + "\n";
                    if (selectedBuilding instanceof HittableBuilding){
                        display = display.concat("hitpoints : " + ((HittableBuilding) selectedBuilding).getHitPoints());
                    }
                    infoLabel.setText(display);
                }
                mercenaryLabel.setText("Mercenaries : " + environnement.getMercenaryCount() + " /" + environnement.getMaxMercenaries());
                ultraVioletsLabel.setText("UltraViolets : " + environnement.getUltraVioletCount());
                civilianLabel.setText("Civilians : " + environnement.getCivilianCount());

                //button activation / deactivation
                if (selectedBuilding instanceof NeutralBuilding){
                    tacosButton.setDisable(false);
                    campButton.setDisable(false);
                } else {
                    tacosButton.setDisable(true);
                    campButton.setDisable(true);
                }

                tacosButton.setText("Convert to Tacos (" + environnement.tacosPrice + ")");
                campButton.setText("Convert to Camp (" + environnement.campPrice + ")");


                //debug display
                if (Config.isDebug && Game.isDebug){
                    gc.setStroke(Color.RED);

                    for (MexicanManifestation mexican : environnement.getMexicanManifestations()){
                        Rectangle2D rectangle = mexican.getVisionRectangle();
                        gc.strokeRect(
                                rectangle.getMinX() - camera.getPosition().getX(),
                                rectangle.getMinY() - camera.getPosition().getY(),
                                rectangle.getWidth(),
                                rectangle.getHeight()
                        );
                    }

                    this.drawQuadtree(environnement.getQuadtree());
                }

                if (environnement.isLost()) {
                    sceneManager.next();
                }
            }
            private void drawQuadtree(Quadtree quadtree){
                LinkedList<Quadtree> children = quadtree.getChildren();
                if (children.size() == 0)
                    return;

                gc.strokeLine(
                        quadtree.square.getMinX() - camera.getPosition().getX(),
                        quadtree.getCenter().getY() - camera.getPosition().getY(),
                        quadtree.square.getMaxX() - camera.getPosition().getX(),
                        quadtree.getCenter().getY() - camera.getPosition().getY()
                );
                gc.strokeLine(
                        quadtree.getCenter().getX() - camera.getPosition().getX(),
                        quadtree.square.getMinY() - camera.getPosition().getY(),
                        quadtree.getCenter().getX() - camera.getPosition().getX(),
                        quadtree.square.getMaxY() - camera.getPosition().getY()
                );
                for (Quadtree child : children){
                    drawQuadtree(child);
                }
            }
        }.start();
    }

    public void setEnvironnement(Environnement environnement){
        this.environnement = environnement;
    }

    @Override
    public void stop(){
        Boot.getExiter().exit();
    }

}
