package main;

import javafx.application.Application;
import javafx.stage.Stage;
import scenemanager.SceneManager;

public class Main extends Application {

    SceneManager sceneManager;

    @Override
    public void start(Stage primaryStage) {
        this.sceneManager = new SceneManager("/controller.godwin", primaryStage);
    }

    @Override
    public void stop(){
        this.sceneManager.getCurrentSceneController().stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
