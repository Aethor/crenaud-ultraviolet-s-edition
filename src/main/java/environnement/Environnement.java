package environnement;

import agents.mexicans.*;
import agents.sombreragram.RaptorTacos;
import environnement.city.*;
import environnement.events.*;
import environnement.sombreragram.SombreraGram;
import io.sarl.core.*;
import io.sarl.lang.annotation.PerceptGuardEvaluator;
import io.sarl.lang.annotation.SarlSpecification;
import io.sarl.lang.core.Agent;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import main.Config;
import main.Game;
import quadtree.Quadtree;
import quadtree.Spatialisable;

import java.util.*;

@SarlSpecification("0.9")
public class Environnement extends Agent {

    public static boolean isDebug = true;
    public static int timeBetweenWaves = 70000;
    public static int updateDuration = 50;
    public static int ultraVioletsPerWave = 7;
    public static int initialCivilianNumber = 100;

    public int tacosPrice = 60;
    public int tacosPriceIncrement = 10;
    public int campPrice = 150;
    public int campPriceIncrement = 5;

    public static int mercenariesPerCamp = 10;
    public static boolean isUltraViolent = false;

    private SombreraGram sombreraGram;

    private Quadtree quadtree;
    private TreeMap<UUID, MexicanManifestation> mexicanContainerTreeMap;
    private City city;
    private Building selectedBuilding = null;
    private boolean isLost = false;
    private int money = 200;

    private int ultraVioletCount = 0;
    private int civilianCount = 0;
    private int mercenaryCount = 0;

    public Environnement(UUID parentID, UUID agentID) {
        super(parentID, agentID);
    }

    /**
     * Initializer
     * @param occurrence
     * @implNote occurence.parameters[0] : Game : Game controller reference
     * @implNote occurence.parameters[1] : int : numbers of UltraViolet to spawn
     * @implNote occurence.parameters[2] : double : canvas width
     * @implNote occurence.parameters[3] : double : canvas height
     * @implNote occurence.parameters[4] : boolean : isUltraViolent
     */
    private void onInitialize(final Initialize occurrence){
        if (Config.isDebug && Environnement.isDebug)
            this.money = 100000;

        isUltraViolent = (boolean) occurrence.parameters[4];

        this.city = new City();

        this.quadtree = new Quadtree(
                city.getDimensions(),
                4,
                7
        );
        for (Building building : this.city.buildings){
            this.quadtree.insert(building);
        }

        this.sombreraGram = new SombreraGram();
        this.getSkill(Lifecycle.class).spawn(RaptorTacos.class, sombreraGram, city);

        this.mexicanContainerTreeMap = new TreeMap<>();

        this.getSkill(Schedules.class).every(updateDuration, agent -> {this.update();});
        this.getSkill(Schedules.class).every(50, a -> {this.emitPerception();});
        this.getSkill(Schedules.class).every(5000, a -> {this.spawnMercenaries();});
        this.getSkill(Schedules.class).every(5000, a -> {this.generateMoney();});
        this.getSkill(Schedules.class).every(timeBetweenWaves, agent -> {this.spawnUltraViolets();});
        this.spawnCivilians();

        ((Game) occurrence.parameters[0]).setEnvironnement(this);
    }
    @PerceptGuardEvaluator
    private void registerInitialize(final Initialize occurence, final Collection<Runnable> eventCollection){
        eventCollection.add(() -> this.onInitialize(occurence));
    }

    private void emitPerception() {
        synchronized (this){
            for (MexicanManifestation mexican : this.getMexicanManifestations()) {
                ArrayList<Spatialisable> collidingObjects = this.quadtree.getCollidingObjects(mexican.getVisionRectangle());
                collidingObjects.remove(mexican);
                this.getSkill(DefaultContextInteractions.class).emit(new Perception(collidingObjects), a -> {
                    return (a.getUUID() == mexican.getUUID());
                });
            }
        }
    }
    public int getMaxMercenaries() {
        int campCount = 0;
        for (Building b : city.buildings) {
            if ("Camp".equals(b.getTypeAsString())){
                campCount += 1;
            }
        }
        return campCount * mercenariesPerCamp;
    }
    private void spawnMercenaries(){
        synchronized (this) {
            for (Building building : this.city.buildings) {
                if (building instanceof Camp) {
                    if(getMaxMercenaries() > getMercenaryCount()) {
                        this.spawnAgent(
                                Mercenary.class,
                                this.getClosestUnusedLocation(
                                        new Rectangle2D(
                                                building.getCollider().getMaxX(),
                                                building.getCollider().getMaxY(),
                                                MexicanManifestation.width,
                                                MexicanManifestation.width
                                        ),
                                        10
                                ),
                                new Object[]{}
                        );
                    }
                }
            }
        }
    }

    private void spawnCivilians(){
        Point2D cityCenter = this.city.getCenter();
        for (int i = 0; i < initialCivilianNumber; i++){
            Building firstBuilding = city.getRandomBuilding();
            Building secondBuilding = city.getRandomBuilding(b -> {return b != firstBuilding;});
            this.spawnAgent(
                    Civilian.class,
                    this.getClosestUnusedLocation(
                            new Rectangle2D(
                                    cityCenter.getX(),
                                    cityCenter.getY(),
                                    MexicanManifestation.width,
                                    MexicanManifestation.width
                            ),
                            50
                    ),
                    new Object[] {firstBuilding, secondBuilding}
            );
        }
    }

    private void spawnUltraViolets(){
        for (int i = 0; i < ultraVioletsPerWave; i++){
            double theoricalXpos = i * MexicanManifestation.width + i * 80;
            this.spawnAgent(
                    UltraViolet.class,
                    new Point2D(
                            theoricalXpos % this.quadtree.square.getMaxX(),
                             i%2 == 0 ? 10 + Math.floor ((MexicanManifestation.width + 80) * (theoricalXpos / this.quadtree.square.getMaxX())) :
                                     this.quadtree.square.getMaxY() - 10 - Math.floor ((MexicanManifestation.width + 80) * (theoricalXpos / this.quadtree.square.getMaxX()))

                    ),
                    new Object[]{this.sombreraGram}
            );
        }
        ultraVioletsPerWave*=1.5;
    }

    private void generateMoney(){
        this.money += 1;
        for (Building building : this.city.buildings){
            if (building instanceof Tacos){
                this.money += 5;
            }
        }
    }

    private void spawnAgent(Class<? extends Agent> clazz, Point2D position, Object[] args){
        MexicanManifestationProxy mexicanManifestationProxy = new MexicanManifestationProxy();
        MexicanManifestation mexicanManifestation = new MexicanManifestation(
                this.getSkill(Lifecycle.class).spawn(clazz, mexicanManifestationProxy, args),
                position,
                clazz,
                isUltraViolent
        );
        synchronized (this){
            this.quadtree.insert(mexicanManifestation);
            this.mexicanContainerTreeMap.put(mexicanManifestation.getUUID(), mexicanManifestation);

            if (clazz == Civilian.class){
                this.civilianCount += 1;
            } else if (clazz == Mercenary.class){
                this.mercenaryCount += 1;
            } else if (clazz == UltraViolet.class){
                this.ultraVioletCount += 1;
            }
        }
        mexicanManifestationProxy.setMexicanManifestation(mexicanManifestation);
    }

    private void update(){
        synchronized (this){
            for (MexicanManifestation mexican : this.getMexicanManifestations()){
                Point2D translation = mexican.getDirection().multiply(updateDuration * MexicanManifestation.speedNorm / 1000);
                ArrayList<Spatialisable> collidingObjects = this.quadtree.getCollidingObjects(mexican.getProjectedCollider(translation));
                if (collidingObjects.isEmpty() || ( collidingObjects.size() == 1 && collidingObjects.get(0) == mexican )) {
                this.quadtree.move(mexican, mexican.getPosition().add(translation));
                } else {
                    this.quadtree.move(mexican, getClosestUnusedLocation(mexican.getCollider(), 0));
                }
            }
        }
    }

    private void onDirectionUpdate(final DirectionUpdate occurence){
        if (!this.mexicanContainerTreeMap.containsKey(occurence.getSource().getUUID()))
            return;
        MexicanManifestation requester = this.mexicanContainerTreeMap.get(occurence.getSource().getUUID());
        requester.setDirection(occurence.direction);
    }
    @PerceptGuardEvaluator
    private void registerDirectionUpdate(final DirectionUpdate occurence, final Collection<Runnable> eventCollection){
        eventCollection.add(() -> this.onDirectionUpdate(occurence));
    }

    private void onHitEvent(final HitEvent occurence){
        MexicanManifestation target = this.mexicanContainerTreeMap.get(occurence.targetId);
        target.hitPoints -= occurence.damage;

        if (target.hitPoints <= 0){
            synchronized (this){
                this.quadtree.delete(target);
                this.mexicanContainerTreeMap.remove(occurence.targetId);
                this.getSkill(DefaultContextInteractions.class).emit(new KillRequest(), address -> {
                    return address.getUUID() == occurence.targetId;
                });
                if (target.getAgentType() == Civilian.class && civilianCount > 0){
                    civilianCount -= 1;
                } else if (target.getAgentType() == Mercenary.class && mercenaryCount > 0){
                    mercenaryCount -= 1;
                } else if (target.getAgentType() == UltraViolet.class && ultraVioletCount > 0){
                    ultraVioletCount -= 1;
                }
                if (civilianCount <= 0){
                    this.isLost = true;
                }
            }
        }
    }

    @PerceptGuardEvaluator
    private void registerHitEvent(final HitEvent occurence, final Collection<Runnable> eventCollection){
        eventCollection.add(() -> this.onHitEvent(occurence));
    }

    private void onBuildingHitEvent(final BuildingHitEvent occurence){
        occurence.building.hit(occurence.damage);

        if (occurence.building.isDestroyed()){
           synchronized (this){
               this.city.removeBuilding((Building) occurence.building);
               this.quadtree.delete((Spatialisable) occurence.building);
               if (occurence.building instanceof Palace){
                   this.isLost = true;
               }
           }
        }
    }
    @PerceptGuardEvaluator
    private void registerBuildingHitEvent(final BuildingHitEvent occurence, final Collection<Runnable> eventCollection){
        eventCollection.add(() -> this.onBuildingHitEvent(occurence));
    }

    private Point2D getClosestUnusedLocation(Rectangle2D initialCollider, double offset){
        synchronized (this){
            if (this.quadtree.getCollidingObjects(initialCollider).isEmpty())
                return new Point2D(initialCollider.getMinX(), initialCollider.getMinY());

            int dist = 1;
            while (true){
                for (int i = -dist; i < dist + 1; i++){
                    for (int j = -dist; j < dist + 1; j++){
                        if (!(i == 0 && j == 0)){

                            Point2D currentPosition = new Point2D(
                                    initialCollider.getMinX() + i * initialCollider.getWidth() + offset * i,
                                    initialCollider.getMinY() + j * initialCollider.getHeight() + offset * j
                            );

                            ArrayList<Spatialisable> collidingObjetcs = this.quadtree.getCollidingObjects(
                                    new Rectangle2D(
                                            currentPosition.getX(),
                                            currentPosition.getY(),
                                            initialCollider.getWidth(),
                                            initialCollider.getHeight()
                                    )
                            );

                            if (collidingObjetcs.isEmpty()){
                                return currentPosition;
                            }
                        }
                    }
                }
                dist++;
            }
        }
    }

    public void convertToTacos(NeutralBuilding building){
        synchronized (this) {
            if (!(this.money >= tacosPrice))
                return;
            this.money -= tacosPrice;
            Rectangle2D collider = building.getCollider();
            this.city.removeBuilding(building);
            Tacos tacos = new Tacos(collider);
            this.city.addBuilding(tacos);
            this.quadtree.insert(tacos);
            tacosPrice += tacosPriceIncrement;
            tacosPriceIncrement += 5;
        }
    }

    public void convertToCamp(NeutralBuilding building){
        synchronized (this){
            if (!(this.money >= campPrice))
                return;
            this.money -= campPrice;
            Rectangle2D collider = building.getCollider();
            this.city.removeBuilding(building);
            Camp camp = new Camp(collider);
            this.city.addBuilding(camp);
            this.quadtree.insert(camp);
            campPrice += campPriceIncrement;
            campPriceIncrement += 5;
        }
    }

    public ArrayList<MexicanManifestation> getMexicanManifestations(){
        synchronized (this){ //sir, this is bad !
            return new ArrayList<>(this.mexicanContainerTreeMap.values());
        }
    }

    public void selectBuilding(Building building){
        this.selectedBuilding = building;
    }

    public Building getClosestBuilding(Point2D position, double maxDist){
        if (city.buildings.isEmpty())
            return null;

        Building closestBuilding = city.buildings.get(0);
        double closestDist = position.distance(closestBuilding.getCenter());
        for (Building building : city.buildings){
            double dist = position.distance(building.getCenter());
            if (dist < closestDist){
                closestDist = dist;
                closestBuilding = building;
            }
        }
        if (closestDist > maxDist)
            return null;
        return closestBuilding;
    }

    public boolean isLost(){
        return isLost;
    }

    public int getUltraVioletCount(){
        return ultraVioletCount;
    }

    public int getCivilianCount(){
        return civilianCount;
    }

    public int getMercenaryCount(){
        return mercenaryCount;
    }

    public Building getSelectedBuilding(){
        return this.selectedBuilding;
    }

    public City getCity(){
        return this.city;
    }

    public SombreraGram getSombreraGram(){
        return this.sombreraGram;
    }

    public Quadtree getQuadtree(){
        return this.quadtree;
    }

    public int getMoney(){
        return money;
    }
}
