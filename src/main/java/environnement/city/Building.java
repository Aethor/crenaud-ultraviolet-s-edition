package environnement.city;

import camera.MovableJfxCamera;
import displayable.Displayable;
import displayable.JfxSprite;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import quadtree.Spatialisable;

public abstract class Building implements Spatialisable, Displayable<MovableJfxCamera> {
    private Rectangle2D collider;
    protected JfxSprite sprite;

    public Building(Rectangle2D collider){
        this.collider = collider;
    }

    public Rectangle2D getCollider(){
        return this.collider;
    }

    public Point2D getCenter(){
        return new Point2D(this.collider.getMinX() + this.collider.getWidth()/2, this.collider.getMinY() + this.collider.getHeight()/2);
    }

    public Point2D getPosition(){
        return new Point2D(this.collider.getMinX(), this.collider.getMinY());
    }

    public void setPosition(Point2D newPosition){
        this.collider = new Rectangle2D(
                newPosition.getX(),
                newPosition.getY(),
                this.collider.getHeight(),
                this.collider.getWidth()
        );
    }

    public String getTypeAsString(){
        return "Building";
    }

    public void display(MovableJfxCamera camera){
        this.sprite.display(camera);
    }

}
