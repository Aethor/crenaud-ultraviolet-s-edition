package environnement.city;

public interface HittableBuilding {
    public void hit(int damage);
    public int getHitPoints();
    public boolean isDestroyed();
}
