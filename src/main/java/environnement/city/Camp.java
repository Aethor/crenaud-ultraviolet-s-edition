package environnement.city;

import displayable.JfxSprite;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;

public class Camp extends Building implements HittableBuilding{
    private int hitPoints = 50;
    public Camp(Rectangle2D rectangle2D) {
        super(rectangle2D);
        this.sprite = new JfxSprite(this.getPosition(), new Image("/blue.png"));
    }

    @Override
    public String getTypeAsString(){
        return "Camp";
    }

    @Override
    public void hit(int damage) {
        hitPoints -= damage;
    }

    @Override
    public int getHitPoints() {
        return hitPoints;
    }

    @Override
    public boolean isDestroyed() {
        return (hitPoints <= 0);
    }
}
