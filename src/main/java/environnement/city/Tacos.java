package environnement.city;

import displayable.JfxSprite;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;

public class Tacos extends Building implements HittableBuilding{
    private int hitPoints = 50;
    public Tacos(Rectangle2D collider) {
        super(collider);
        this.sprite = new JfxSprite(this.getPosition(), new Image("/orange.png"));
    }

    @Override
    public String getTypeAsString(){
        return "Tacos";
    }

    @Override
    public void hit(int damage) {
        hitPoints -= damage;
    }

    @Override
    public int getHitPoints() {
        return hitPoints;
    }

    @Override
    public boolean isDestroyed() {
        return hitPoints <= 0;
    }
}
