package environnement.city;

import displayable.JfxSprite;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;

public class NeutralBuilding extends Building{
    public NeutralBuilding(Rectangle2D collider) {
        super(collider);
        this.sprite = new JfxSprite(this.getPosition(), new Image("/black.png"));
    }

    @Override
    public String getTypeAsString(){
        return "Neutral Building";
    }
}
