package environnement.city;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class City {
    private static int outsideZoneSize = 10;
    private static int cityZoneSize = 40;
    private static int centerZoneWidth = 10;
    private static double density = 0.8;

    public static int buildingWidth = 40;

    public ArrayList<Building> buildings;
    public int width;
    public int height;

    public City(){
        this.buildings = new ArrayList<>();
        this.width = (2 * outsideZoneSize + 2 * cityZoneSize + centerZoneWidth);
        this.height = (2 * outsideZoneSize + 2 * cityZoneSize + centerZoneWidth);

        Palace palace = new Palace(
                new Rectangle2D(
                        (outsideZoneSize + cityZoneSize + Math.abs(centerZoneWidth/2)) * buildingWidth,
                        (outsideZoneSize + cityZoneSize + Math.abs(centerZoneWidth/2)) * buildingWidth,
                        buildingWidth,
                        buildingWidth
                )
        );
        this.buildings.add(palace);

        Tacos tacos = new Tacos(
                new Rectangle2D(
                        (outsideZoneSize + cityZoneSize) * buildingWidth,
                        (outsideZoneSize + cityZoneSize) * buildingWidth,
                        buildingWidth,
                        buildingWidth
                )
        );
        this.buildings.add(tacos);
        Camp camp = new Camp(
                new Rectangle2D(
                        (outsideZoneSize + cityZoneSize + centerZoneWidth) * buildingWidth,
                        (outsideZoneSize + cityZoneSize + centerZoneWidth) * buildingWidth,
                        buildingWidth,
                        buildingWidth
                )
        );
        this.buildings.add(camp);

        int i, j;

        int roadSize = 2;
        int blocSize = 3;

        for (i = outsideZoneSize; i < this.width - outsideZoneSize; i++){
            for (j = outsideZoneSize; j < this.height - outsideZoneSize; j++){
                // avoid center
                if(distToCenter( new Point2D(i * buildingWidth,j * buildingWidth)) < centerZoneWidth*buildingWidth){
                    continue;
                }

                // create streets
                if(i%blocSize > Math.random()*roadSize &&
                        i%blocSize < blocSize - Math.random()*roadSize &&
                        j%blocSize > Math.random()*roadSize &&
                        j%blocSize < blocSize - Math.random()*roadSize) {
                    // apply density with a repartition
                    if ((Math.random() < density * (20000*buildingWidth)/Math.pow(distToCenter( new Point2D(i * buildingWidth,j * buildingWidth)),2))) {
                        NeutralBuilding neutralBuilding = new NeutralBuilding(
                                new Rectangle2D(
                                        i * buildingWidth,
                                        j * buildingWidth,
                                        buildingWidth,
                                        buildingWidth
                                )
                        );
                        this.buildings.add(neutralBuilding);
                    }
                }
            }

        }
    }

    double distToCenter(Point2D point){
        return getCenter().distance(point);
    }

    public Rectangle2D getDimensions(){
        return new Rectangle2D(
                0,
                0,
                this.width * buildingWidth,
                this.height * buildingWidth
        );
    }

    public Point2D getCenter(){
        return new Point2D(this.width * buildingWidth / 2, this.height * buildingWidth / 2);
    }

    public void removeBuilding(Building building){
        this.buildings.remove(building);
    }

    public void addBuilding(Building building){
        this.buildings.add(building);
    }

    public Building getRandomBuilding(){
        return this.getRandomBuilding(a -> {return true;});
    }

    public Building getRandomBuilding(Predicate<Building> filter){
        List<Building> filteredBuildings = this.buildings.stream().filter(filter).collect(Collectors.toList());
        if (filteredBuildings.isEmpty())
            return null;
        return filteredBuildings.get((new Random()).nextInt(filteredBuildings.size()));
    }
}
