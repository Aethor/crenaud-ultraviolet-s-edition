package environnement.events;

import io.sarl.lang.core.Event;
import javafx.geometry.Point2D;

public class DirectionUpdate extends Event {
    public Point2D direction;
    public DirectionUpdate(Point2D direction){
        this.direction = direction.normalize();
    }
}
