package environnement.events;

import io.sarl.lang.core.Event;
import quadtree.Spatialisable;

import java.util.ArrayList;

public class Perception extends Event {
    public ArrayList<Spatialisable> perceivedEntities;
    public Perception(ArrayList<Spatialisable> perceivedEntities){
        this.perceivedEntities = perceivedEntities;
    }
}
