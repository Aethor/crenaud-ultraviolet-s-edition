package environnement.events;

import io.sarl.lang.core.Event;

import java.util.UUID;

public class HitEvent extends Event {
    public UUID targetId;
    public int damage;

    public HitEvent(UUID targetId, int damage){
        this.targetId = targetId;
        this.damage = damage;
    }
}
