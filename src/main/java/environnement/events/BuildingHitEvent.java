package environnement.events;

import environnement.city.Building;
import environnement.city.HittableBuilding;
import io.sarl.lang.core.Event;

public class BuildingHitEvent extends Event {
    public HittableBuilding building;
    public int damage;

    public BuildingHitEvent(HittableBuilding building, int damage){
        this.damage = damage;
        this.building = building;
    }
}
