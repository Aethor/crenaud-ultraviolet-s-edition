package environnement.sombreragram;

import java.util.LinkedList;

public class SombreraGram {

    private LinkedList<SombreraPost> sombreraPosts;
    private LinkedList<SombreraPost> lastPosts;
    private SombreraPost activePost = null;

    public SombreraGram(){
        this.sombreraPosts = new LinkedList<>();
        this.lastPosts = new LinkedList<>();
    }

    public void post(SombreraPost post){
        this.sombreraPosts.add(post);
        this.lastPosts.add(post);
        this.activePost = this.sombreraPosts.getLast();
    }

    public SombreraPost getActivePost() {
        return this.activePost;
    }

    public LinkedList<SombreraPost> getLastPosts(){
        LinkedList<SombreraPost> savedPosts = (LinkedList<SombreraPost>) this.lastPosts.clone();
        this.lastPosts.clear();
        return savedPosts;
    }
}
