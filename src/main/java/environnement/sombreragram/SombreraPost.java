package environnement.sombreragram;

import environnement.city.Building;

public class SombreraPost {

    private String content;
    private Building targetBuilding;

    public SombreraPost(String content){
        this.content = content;
        this.targetBuilding = null;
    }

    public SombreraPost(String content, Building targetBuilding){
        this.content = content;
        this.targetBuilding = targetBuilding;
    }

    public String getContent() {
        return content;
    }

    public Building getTargetBuilding(){
        return targetBuilding;
    }

    public String toString(){
        String bakedText = "@RaptorTacos : \n";
        if (getTargetBuilding() == null)
            bakedText += getContent() + "\n";
        else {
            bakedText += "Destroy the " + getTargetBuilding().getTypeAsString().toLowerCase();
        }
        return bakedText;
    }
}
