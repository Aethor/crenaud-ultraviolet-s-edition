package agents.sombreragram;

import environnement.city.City;
import environnement.city.NeutralBuilding;
import environnement.sombreragram.SombreraGram;
import environnement.sombreragram.SombreraPost;
import io.sarl.core.Initialize;
import io.sarl.core.Schedules;
import io.sarl.lang.annotation.PerceptGuardEvaluator;
import io.sarl.lang.annotation.SarlSpecification;
import io.sarl.lang.core.Agent;

import java.util.Collection;
import java.util.UUID;

@SarlSpecification("0.9")
public class RaptorTacos extends Agent {

    public static int timeBetweenPosts = 15000;

    private SombreraGram sombreraGram;
    private City city;

    static private String[] possibleMessages = {
            "This governement is a joke !",
            "We UltraViolet wont accept this ! Destroy everything !",
            "Death to @RealJimmyNeutral !",
            "I may love plastic, but this governement is bad",
            "Glory to China ! Wait..",
            "Protect your privacy with @VioletVPN_official",
            "Learn how to riot today, on Brilliant.NotSponsoredByUltraViolets.com",
            "Cooking is easy : just do it the #standard way !",
            "Jimmy neutral is not so neutral it seems @RealJimmyNeutral",
            "Jimmy neutral is too neutral @RealJimmyNeutral",
            "Please follow @RaptorTacos",
            "Sorry about the last tweet - got hacked",
            "@JusticeSombrero we wont let the governement get away !",
            "Dont forget to put on your invisibility devices #violet",
            "We are peaceful, but this is enough @RealJimmyNeutral",
            "@JimmyNatural is better than @RealJimmyNeutral",
            "@ElGrande @VioletMaster we need to stop this",
            "We never forgot @RealDonaldJTrump",
            "@JusticeSombrero is the parangon of neutrality",
            "Thank you everyone for the #SombreraGramAward",
            "Please help me, I'm trapped in an IA51 project",
            "Last #SmashDLC was bad. Blame the governement !",
            "Remember : we #UltraViolet fight for social justice",
            "@RealJimmyNeutral surrender, you can't win",
            "#JailSchool is so good",
            "unpopular opinion : #SAO wasn't THAT bad",
            "she was 300y.o. officer",
            "#PesosShaveClub, for great skin and good health ! #nosponsor",
            "Make your own website on @violetwix_com now ! #nosponsor"
    };

    public RaptorTacos(UUID parentID, UUID agentID) {
        super(parentID, agentID);
    }

    /**
     * Initializer
     * @param occurence
     * @implNote occurence.parameters[0] : SombreraGram : reference to SombreraGram
     * @implNote occurence.parameters[1] : City : reference to the City (to targetId buildings)
     */
    private void onInitialize(final Initialize occurence){
        this.sombreraGram = (SombreraGram) occurence.parameters[0];
        this.city = (City) occurence.parameters[1];
        this.post();
        this.getSkill(Schedules.class).in((long) (Math.random() * timeBetweenPosts), agent -> {this.post();});
    }
    @PerceptGuardEvaluator
    private void registerInitialize(final Initialize occurence, final Collection<Runnable> eventCollection){
        eventCollection.add(() -> this.onInitialize(occurence));
    }

    private void post(){
        if (Math.random() * 3 > 1){
            this.sombreraGram.post(
                    new SombreraPost(
                            possibleMessages[(int) (Math.random() * possibleMessages.length)],
                            this.city.getRandomBuilding(building -> {
                                return (!(building instanceof NeutralBuilding));
                            })
                    )
            );
        }
        else {
            this.sombreraGram.post(new SombreraPost(possibleMessages[(int) (Math.random() * possibleMessages.length)]));
        }

        this.getSkill(Schedules.class).in((long) (Math.random() * timeBetweenPosts), agent -> {this.post();});
    }
}
