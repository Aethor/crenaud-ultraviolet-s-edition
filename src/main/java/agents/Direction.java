package agents;

import javafx.geometry.Point2D;

public class Direction {
    public static boolean isRight(Point2D pos){
        return (pos.getX() > 0 && pos.getY() == 0);
    }
    public static boolean isLeft(Point2D pos){
        return (pos.getX() < 0 && pos.getY() == 0);
    }
    public static boolean isUp(Point2D pos){
        return (pos.getX() == 0 && pos.getY() < 0);
    }
    public static boolean isDown(Point2D pos){
        return (pos.getX() == 0 && pos.getY() > 0);
    }
}
