package agents.mexicans;

import io.sarl.lang.annotation.SarlSpecification;

import java.util.UUID;

@SarlSpecification("0.9")
public class President extends Mexican{
    public President(UUID parentID, UUID agentID) {
        super(parentID, agentID);
    }
}
