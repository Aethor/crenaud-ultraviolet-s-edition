package agents.mexicans;

import agents.mexicans.behaviours.BasicHitSkill;
import agents.mexicans.behaviours.BasicMoveSkill;
import agents.mexicans.behaviours.HitCapacity;
import agents.mexicans.behaviours.MoveCapacity;
import environnement.city.Building;
import environnement.events.DirectionUpdate;
import environnement.events.KillRequest;
import environnement.events.Perception;
import io.sarl.core.DefaultContextInteractions;
import io.sarl.core.Initialize;
import io.sarl.lang.annotation.SarlSpecification;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import quadtree.Spatialisable;

import java.util.ArrayList;
import java.util.UUID;

@SarlSpecification("0.9")
public class Mercenary extends Mexican{

    private double maxPatrollLineLength = 600;
    private double minPatrollLineLength = 200;
    private Point2D currentTarget;
    private Rectangle2D patrollRectangle;

    private ArrayList<Class> targets;
    private int hitFrequency = 1;

    private long lastDate;
    private long timeSinceLastHit;

    public Mercenary(UUID parentID, UUID agentID) {
        super(parentID, agentID);
    }

    protected void onInitialize(final Initialize occurence){
        super.onInitialize(occurence);
        this.setSkill(new BasicMoveSkill(), MoveCapacity.class);
        this.setSkill(new BasicHitSkill(), HitCapacity.class);

        this.patrollRectangle = null;

        this.targets = new ArrayList<>();
        targets.add(UltraViolet.class);

        lastDate = System.currentTimeMillis();
        timeSinceLastHit = 0;
    }

    protected void onPerception(final Perception occurence){
        if (this.mexicanManifestationProxy == null)
            return;

        long date = System.currentTimeMillis();

        Point2D currentDirection = mexicanManifestationProxy.getDirection();
        Point2D center = mexicanManifestationProxy.getCenter();
        Rectangle2D collider = mexicanManifestationProxy.getCollider();

        this.timeSinceLastHit = this.getSkill(HitCapacity.class).hitInRange(
                occurence.perceivedEntities,
                this.targets,
                collider,
                this.hitFrequency,
                this.timeSinceLastHit,
                date - lastDate,
                this.getSkill(DefaultContextInteractions.class),
                occurence.getSource().getUUID()
        );

        Point2D direction;

        Point2D ultraVioletsDirection = this.getSkill(MoveCapacity.class).directionToGroup(
                UltraViolet.class,
                occurence.perceivedEntities,
                center
        );

        // chase mode
        if (ultraVioletsDirection.getX() != 0 || ultraVioletsDirection.getY() != 0){
            direction = ultraVioletsDirection;

        }
        // patroll mode
        else {
            if (this.patrollRectangle == null){
                this.generatePatrollRectangle(center);
            }

            if (currentTarget == null){
                currentTarget = new Point2D(patrollRectangle.getMinX(), patrollRectangle.getMinY());
            }
            if (center.distance(currentTarget) < 100){
                this.setNextTarget();
            }

            direction = this.getSkill(MoveCapacity.class).directionToTarget(
                    currentTarget,
                    mexicanManifestationProxy,
                    occurence.perceivedEntities
            );
        }

        this.getSkill(DefaultContextInteractions.class).emit(new DirectionUpdate(direction), address -> {
            return address.getUUID() == occurence.getSource().getUUID();
        });

        lastDate = date;

    }

    private void generatePatrollRectangle(Point2D currentCenter){
        this.patrollRectangle = new Rectangle2D(
                currentCenter.getX() - (Math.random() * (maxPatrollLineLength - minPatrollLineLength) + minPatrollLineLength)/2,
                currentCenter.getY() - (Math.random() * (maxPatrollLineLength - minPatrollLineLength) + minPatrollLineLength)/2,
                Math.random() * (maxPatrollLineLength - minPatrollLineLength) + minPatrollLineLength,
                Math.random() * (maxPatrollLineLength - minPatrollLineLength) + minPatrollLineLength
        );
    }

    private void setNextTarget(){
        if (currentTarget.getX() == patrollRectangle.getMinX() && currentTarget.getY() == patrollRectangle.getMinY()){
            currentTarget = new Point2D(patrollRectangle.getMaxX(), patrollRectangle.getMinY());
        } else if (currentTarget.getX() == patrollRectangle.getMaxX() && currentTarget.getY() == patrollRectangle.getMinY()){
            currentTarget = new Point2D(patrollRectangle.getMaxX(), patrollRectangle.getMaxY());
        } else if (currentTarget.getX() == patrollRectangle.getMaxX() && currentTarget.getY() == patrollRectangle.getMaxY()){
            currentTarget = new Point2D(patrollRectangle.getMinX(), patrollRectangle.getMaxY());
        } else if (currentTarget.getX() == patrollRectangle.getMinX() && currentTarget.getY() == patrollRectangle.getMaxY()){
            currentTarget = new Point2D(patrollRectangle.getMinX(), patrollRectangle.getMinY());
        }
    }

    protected void onKillRequest(KillRequest occurence){
        super.onKillRequest(occurence);
    }
}
