package agents.mexicans.behaviours;

import agents.mexicans.Mexican;
import agents.mexicans.MexicanManifestationProxy;
import environnement.city.Building;
import io.sarl.lang.core.Capacity;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import quadtree.Spatialisable;

import java.util.ArrayList;

public interface MoveCapacity extends Capacity {
    public Point2D directionToBuilding(
            Building building,
            MexicanManifestationProxy mexicanManifestationProxy,
            ArrayList<Spatialisable> perceivedEntities
    );

    public Point2D directionToTarget(
            Point2D target,
            MexicanManifestationProxy mexicanManifestationProxy,
            ArrayList<Spatialisable> perceivedEntities
    );

    public Point2D groupDirection(
            Class<? extends Mexican> groupType,
            ArrayList<Spatialisable> perceivedEntities,
            Rectangle2D collider
    );

    public Point2D directionToGroup(
            Class<? extends Mexican> groupType,
            ArrayList<Spatialisable> perceivedEntities,
            Point2D center
    );
}
