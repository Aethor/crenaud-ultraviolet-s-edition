package agents.mexicans.behaviours;

import io.sarl.core.DefaultContextInteractions;
import io.sarl.lang.core.Capacity;
import javafx.geometry.Rectangle2D;
import quadtree.Spatialisable;

import java.util.ArrayList;
import java.util.UUID;

public interface HitCapacity extends Capacity {
    public long hitInRange(
            ArrayList<Spatialisable> perceivedEntities,
            ArrayList<Class> targets,
            Rectangle2D collider,
            int hitFrequency,
            long timeSinceLastHit,
            long timeOffset,
            DefaultContextInteractions defaultContextInteractions,
            UUID environnementId
    );
}
