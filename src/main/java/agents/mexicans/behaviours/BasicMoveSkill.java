package agents.mexicans.behaviours;

import agents.mexicans.*;
import environnement.city.Building;
import environnement.city.City;
import io.sarl.lang.core.Skill;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import quadtree.Spatialisable;

import java.util.ArrayList;

public class BasicMoveSkill extends Skill implements MoveCapacity {
    @Override
    public Point2D directionToBuilding(
            Building building,
            MexicanManifestationProxy mexicanManifestationProxy,
            ArrayList<Spatialisable> perceivedEntities
    ) {
        return directionToTarget(building.getCenter(), mexicanManifestationProxy, perceivedEntities);
    }

    @Override
    public Point2D directionToTarget(
            Point2D target,
            MexicanManifestationProxy mexicanManifestationProxy,
            ArrayList<Spatialisable> perceivedEntities
    ) {
        if (mexicanManifestationProxy == null || target == null)
            return null;

        Point2D center = mexicanManifestationProxy.getCenter();
        Rectangle2D collider = mexicanManifestationProxy.getCollider();

        Point2D direction = new Point2D(0,0);

        direction = direction.add(target.subtract(center).normalize());

        Point2D obstaclesInfluence = new Point2D(0,0);

        for (Spatialisable entity : perceivedEntities){
            double distance = MexicanManifestation.getMinDistBetweenColliders(collider, entity.getCollider());
            if (distance <= 0 && !entity.getCenter().equals(center)){
                return center.subtract(entity.getCenter()).normalize();
            }
            if (entity instanceof Building && distance < 10){
                obstaclesInfluence = obstaclesInfluence.add(entity.getCenter().subtract(center).multiply(1/distance));
            } else if (entity instanceof MexicanManifestation && distance < 10){
                obstaclesInfluence = obstaclesInfluence.add(entity.getCenter().subtract(center).multiply(1/distance));
            }
        }

        if (obstaclesInfluence.getX() != 0 && obstaclesInfluence.getY() != 0){
            direction = direction.subtract(obstaclesInfluence.normalize());
        }

        return direction.normalize();
    }

    @Override
    public Point2D groupDirection(Class<? extends Mexican> groupType, ArrayList<Spatialisable> perceivedEntities, Rectangle2D collider) {
        Point2D groupInfluence = new Point2D(0,0);
        for (Spatialisable entity : perceivedEntities){
            if (entity instanceof MexicanManifestation && ((MexicanManifestation) entity).getAgentType() == groupType){
                double distance = MexicanManifestation.getMinDistBetweenColliders(collider, entity.getCollider());
                if (distance > 5){
                    groupInfluence = groupInfluence.add(((MexicanManifestation) entity).getDirection().normalize());
                }
            }
        }
        return groupInfluence.normalize();
    }

    @Override
    public Point2D directionToGroup(Class<? extends Mexican> groupType, ArrayList<Spatialisable> perceivedEntities, Point2D center) {
        Point2D direction = new Point2D(0,0);
        for (Spatialisable entity: perceivedEntities){
            if (entity instanceof MexicanManifestation && ((MexicanManifestation) entity).getAgentType() == groupType){
                direction = direction.add(entity.getCenter().subtract(center).normalize());
            }
        }
        return direction.normalize();
    }
}
