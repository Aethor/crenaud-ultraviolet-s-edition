package agents.mexicans.behaviours;

import agents.mexicans.Mexican;
import agents.mexicans.MexicanManifestation;
import environnement.city.HittableBuilding;
import environnement.events.BuildingHitEvent;
import environnement.events.HitEvent;
import io.sarl.core.DefaultContextInteractions;
import io.sarl.lang.core.Skill;
import javafx.geometry.Rectangle2D;
import quadtree.Spatialisable;

import java.util.ArrayList;
import java.util.UUID;

public class BasicHitSkill extends Skill implements HitCapacity {

    @Override
    /**
     *
     */
    public long hitInRange(
            ArrayList<Spatialisable> perceivedEntities,
            ArrayList<Class> targets,
            Rectangle2D collider,
            int hitFrequency,
            long timeSinceLastHit,
            long timeOffset,
            DefaultContextInteractions defaultContextInteractions,
            UUID environnementId
    ) {
        timeSinceLastHit += timeOffset;

        if (((double) 1 / hitFrequency) * 1000 > timeSinceLastHit)
            return timeSinceLastHit;

        if (perceivedEntities.isEmpty())
            return timeSinceLastHit;

        for (Spatialisable entity : perceivedEntities){
            double distance = MexicanManifestation.getMinDistBetweenColliders(collider, entity.getCollider());
            if (distance > 10)
                continue;
            for (Class targetType : targets){
                if (entity instanceof MexicanManifestation && ((MexicanManifestation) entity).getAgentType() == targetType){
                    UUID id = ((MexicanManifestation) entity).getUUID();
                    defaultContextInteractions.emit(new HitEvent(id, 1), address -> {
                        return address.getUUID() == environnementId;
                    });
                    return 0;
                } else if (entity instanceof HittableBuilding && targetType == HittableBuilding.class){
                    defaultContextInteractions.emit(new BuildingHitEvent((HittableBuilding) entity, 1), address -> {
                        return address.getUUID() == environnementId;
                    });
                    return 0;
                }
            }
        }
        return timeSinceLastHit;
    }
}
