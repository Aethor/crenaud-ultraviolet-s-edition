package agents.mexicans;

import agents.mexicans.behaviours.BasicMoveSkill;
import agents.mexicans.behaviours.MoveCapacity;
import environnement.city.Building;
import environnement.events.DirectionUpdate;
import environnement.events.KillRequest;
import environnement.events.Perception;
import io.sarl.core.DefaultContextInteractions;
import io.sarl.core.Initialize;
import io.sarl.core.Logging;
import io.sarl.lang.annotation.SarlSpecification;
import javafx.geometry.Point2D;
import quadtree.Spatialisable;

import java.util.UUID;

@SarlSpecification("0.9")
public class Civilian extends Mexican{

    private Building firstBuilding;
    private Building secondBuilding;
    private Building currentTarget;

    public Civilian(UUID parentID, UUID agentID) {
        super(parentID, agentID);
    }


    /**
     * Initializer
     * @param occurence
     * @implNote occurence.parameters[0] : MexicanManifestationProxy
     * @implNote occurence.parameters[1] : Object[] parameters
     * @implNote occurence.parameters[1][0] : Building firstTargetBuilding
     * @implNote occurence.parameters[1][1] : Building secondTargetBuilding
     */
    protected void onInitialize(final Initialize occurence){
        super.onInitialize(occurence);

        this.firstBuilding = (Building) ((Object[])occurence.parameters[1])[0];
        this.secondBuilding = (Building) ((Object[])occurence.parameters[1])[1];
        this.currentTarget = firstBuilding;
        this.setSkill(new BasicMoveSkill(), MoveCapacity.class);
    }

    protected void onPerception(final Perception occurence){
        if (this.mexicanManifestationProxy == null)
            return;

        for (Spatialisable entity : occurence.perceivedEntities){
            if (entity == currentTarget){
                if (currentTarget == firstBuilding){
                    currentTarget = secondBuilding;
                } else {
                    currentTarget = firstBuilding;
                }
                return;
            }
        }

        Point2D direction = this.getSkill(MoveCapacity.class).directionToBuilding(
                this.currentTarget,
                this.mexicanManifestationProxy,
                occurence.perceivedEntities
        );


        this.getSkill(DefaultContextInteractions.class).emit(new DirectionUpdate(direction), address -> {
            return address.getUUID() == occurence.getSource().getUUID();
        });
    }

    protected void onKillRequest(final KillRequest occurence){
        super.onKillRequest(occurence);
    }
}
