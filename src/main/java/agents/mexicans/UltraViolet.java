package agents.mexicans;

import agents.mexicans.behaviours.BasicHitSkill;
import agents.mexicans.behaviours.HitCapacity;
import agents.mexicans.behaviours.MoveCapacity;
import agents.mexicans.behaviours.BasicMoveSkill;
import environnement.city.Building;
import environnement.city.HittableBuilding;
import environnement.events.DirectionUpdate;
import environnement.events.HitEvent;
import environnement.events.KillRequest;
import environnement.events.Perception;
import environnement.sombreragram.SombreraGram;
import environnement.sombreragram.SombreraPost;
import io.sarl.core.DefaultContextInteractions;
import io.sarl.core.Initialize;
import io.sarl.core.Logging;
import io.sarl.core.Schedules;
import io.sarl.lang.annotation.SarlSpecification;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import main.Config;
import quadtree.Spatialisable;

import java.util.ArrayList;
import java.util.UUID;

@SarlSpecification("0.9")
public class UltraViolet extends Mexican{

    public static boolean isDebug = false;
    private static int maxTimeBetweenRead = 15000;

    private SombreraGram sombreraGram;
    private Building targetBuilding;
    private ArrayList<Class> targets;
    private int hitFrequency = 1;

    private long lastDate;
    private long timeSinceLastHit;

    public UltraViolet(UUID parentID, UUID agentID) {
        super(parentID, agentID);
    }

    /**
     * Initializer
     * @param occurence
     * @implNote occurence.parameters[0] : MexicanManifestationProxy
     * @implNote occurence.parameters[1] : Object[] parameters
     * @implNote occurence.parameters[1][0] : SombreraGram
     */
    protected void onInitialize(final Initialize occurence){
        if (Config.isDebug && UltraViolet.isDebug)
            this.getSkill(Logging.class).info("UltraViolet agent : Launched");

        super.onInitialize(occurence);
        this.sombreraGram = (SombreraGram) ((Object[]) occurence.parameters[1])[0];

        targets = new ArrayList<>();
        targets.add(Civilian.class);
        targets.add(Mercenary.class);
        targets.add(HittableBuilding.class);

        this.getSkill(Schedules.class).in((long)(Math.random() * maxTimeBetweenRead), agent -> {this.readSombreraGram();});
        this.setSkill(new BasicMoveSkill(), MoveCapacity.class);
        this.setSkill(new BasicHitSkill(), HitCapacity.class);

        lastDate = System.currentTimeMillis();
        timeSinceLastHit = 0;
    }

    private void readSombreraGram(){
        SombreraPost activePost = this.sombreraGram.getActivePost();
        if (activePost == null){
            this.getSkill(Schedules.class).in((long)(Math.random() * maxTimeBetweenRead), agent -> {this.readSombreraGram();});
            return;
        }

        this.targetBuilding = activePost.getTargetBuilding();
        this.getSkill(Schedules.class).in((long)(Math.random() * maxTimeBetweenRead), agent -> {this.readSombreraGram();});
    }

    protected void onPerception(final Perception occurence){
        if (Config.isDebug && UltraViolet.isDebug)
            this.getSkill(Logging.class).info("UltraViolet agent : Received perception");

        if (this.mexicanManifestationProxy == null)
            return;

        long date = System.currentTimeMillis();

        Point2D center = mexicanManifestationProxy.getCenter();
        Rectangle2D collider = mexicanManifestationProxy.getCollider();

        timeSinceLastHit = this.getSkill(HitCapacity.class).hitInRange(
                occurence.perceivedEntities,
                this.targets,
                collider,
                this.hitFrequency,
                this.timeSinceLastHit,
                date - lastDate,
                this.getSkill(DefaultContextInteractions.class),
                occurence.getSource().getUUID()
        );

        Point2D direction = new Point2D(0,0);
        if (this.targetBuilding != null) {
            direction = this.getSkill(MoveCapacity.class).directionToBuilding(
                    targetBuilding,
                    mexicanManifestationProxy,
                    occurence.perceivedEntities
            );
        }

        Point2D alliedInfluence = this.getSkill(MoveCapacity.class).groupDirection(
                UltraViolet.class,
                occurence.perceivedEntities,
                collider
        );

        direction = direction.add(alliedInfluence.multiply(0.25));

        this.getSkill(DefaultContextInteractions.class).emit(new DirectionUpdate(direction.normalize()), address -> {
            return address.getUUID() == occurence.getSource().getUUID();
        });

        lastDate = date;
    }

    protected void onKillRequest(KillRequest occurence){
        super.onKillRequest(occurence);
    }
}
