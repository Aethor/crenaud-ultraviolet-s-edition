package agents.mexicans;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;

public class MexicanManifestationProxy {
    private MexicanManifestation mexicanManifestation;

    public MexicanManifestationProxy(){
        this.mexicanManifestation = null;
    }

    public MexicanManifestationProxy(MexicanManifestation mexicanManifestation){
        this.mexicanManifestation = mexicanManifestation;
    }

    public void setMexicanManifestation(MexicanManifestation mexicanManifestation){
        this.mexicanManifestation = mexicanManifestation;
    }

    public boolean isInitialized(){
        return (this.mexicanManifestation != null);
    }

    public Point2D getDirection(){
        if (this.mexicanManifestation == null)
            return null;
        return new Point2D(
                mexicanManifestation.getDirection().getX(),
                mexicanManifestation.getDirection().getY()
        );
    }

    public Point2D getPosition(){
        if (this.mexicanManifestation == null)
            return null;
        return new Point2D(
                mexicanManifestation.getPosition().getX(),
                mexicanManifestation.getPosition().getY()
        );
    }

    public Rectangle2D getCollider(){
        if (this.mexicanManifestation == null)
            return null;
        return new Rectangle2D(
                mexicanManifestation.getCollider().getMinX(),
                mexicanManifestation.getCollider().getMinY(),
                mexicanManifestation.getCollider().getWidth(),
                mexicanManifestation.getCollider().getHeight()
        );
    }

    public Point2D getCenter(){
        if (this.mexicanManifestation == null)
            return null;
        return new Point2D(
                mexicanManifestation.getCenter().getX(),
                mexicanManifestation.getCenter().getY()
        );
    }
}
