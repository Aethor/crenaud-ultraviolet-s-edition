package agents.mexicans;

import environnement.events.KillRequest;
import environnement.events.Perception;
import io.sarl.core.Initialize;
import io.sarl.core.Lifecycle;
import io.sarl.lang.annotation.PerceptGuardEvaluator;
import io.sarl.lang.annotation.SarlSpecification;
import io.sarl.lang.core.Agent;

import java.util.Collection;
import java.util.UUID;

@SarlSpecification("0.9")
public abstract class Mexican extends Agent {
    protected MexicanManifestationProxy mexicanManifestationProxy;
    public Mexican(UUID parentID, UUID agentID) {
        super(parentID, agentID);
    }

    protected void onInitialize(final Initialize occurence){
        this.mexicanManifestationProxy = (MexicanManifestationProxy) occurence.parameters[0];
    }
    @PerceptGuardEvaluator
    private void registerInitialize(final Initialize occurence, final Collection<Runnable> eventCollection){
        eventCollection.add(() -> this.onInitialize(occurence));
    }

    @PerceptGuardEvaluator
    private void registerPerception(final Perception occurence, final Collection<Runnable> eventCollection){
        eventCollection.add(() -> this.onPerception(occurence));
    }
    protected void onPerception(final Perception occurence){
    }

    @PerceptGuardEvaluator
    private void registerKillRequest(final KillRequest occurence, final Collection<Runnable> eventCollection){
        eventCollection.add(() -> this.onKillRequest(occurence));
    }
    protected void onKillRequest(final KillRequest occurence){
        this.getSkill(Lifecycle.class).killMe();
    }
}
