package agents.mexicans;

import agents.Direction;
import camera.MovableJfxCamera;
import displayable.Displayable;
import displayable.JfxSprite;
import io.sarl.lang.core.Agent;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import quadtree.Spatialisable;

import java.util.UUID;

public class MexicanManifestation implements Spatialisable, Displayable<MovableJfxCamera> {

    private UUID uuid;

    public JfxSprite sprite;

    public static int width = 20;
    public static int visionRadius = 60;
    public static double speedNorm = 50;

    public int hitPoints = 10;

    private Rectangle2D collider;
    private Point2D direction;
    private Class<? extends Agent> agentType;

    public MexicanManifestation(UUID uuid, Point2D position, Class<? extends Agent> clazz, boolean isUltraViolent){
        this.uuid = uuid;
        this.collider = new Rectangle2D(
                position.getX(),
                position.getY(),
                width,
                width
        );

        String spritePath;
        if (clazz == UltraViolet.class)
            spritePath = "/purple.png";
        else if (clazz == Civilian.class)
            spritePath = "/black.png";
        else if (clazz == Mercenary.class)
            spritePath = "/blue.png";
        else if (clazz == President.class)
            spritePath = "/green.png";
        else {
            System.out.println("warning : no sprite found for mexican type " + clazz + ", using default.");
            spritePath = "/black.png";
        }
        this.agentType = clazz;

        this.sprite = new JfxSprite(this.getPosition(), new Image(spritePath, width, width, false, false));
        this.direction = new Point2D(0,0);

        if (clazz == UltraViolet.class && isUltraViolent)
            this.sprite = null;
    }

    @Override
    public Rectangle2D getCollider() {
        return this.collider;
    }

    public Rectangle2D getVisionRectangle(){
        return new Rectangle2D(
            this.getCenter().getX() - visionRadius,
                this.getCenter().getY() - visionRadius,
                visionRadius * 2,
                visionRadius * 2
        );
    }

    public Rectangle2D getProjectedCollider(Point2D translation){
        return new Rectangle2D(
                collider.getMinX() + translation.getX(),
                collider.getMinY() + translation.getY(),
                collider.getWidth(),
                collider.getHeight()
        );
    }

    @Override
    public Point2D getCenter() {
        return new Point2D(
                this.collider.getMinX() + this.collider.getWidth()/2,
                this.collider.getMinY() + this.collider.getHeight()/2
        );
    }

    public Point2D getPosition(){
        return new Point2D(this.collider.getMinX(), this.collider.getMinY());
    }

    public Point2D getDirection(){return this.direction;}

    @Override
    public void setPosition(Point2D newPosition) {
        this.collider = new Rectangle2D(
                newPosition.getX(),
                newPosition.getY(),
                this.collider.getWidth(),
                this.collider.getHeight()
        );
        if (this.sprite == null)
            return;
        this.sprite.setPosition(new Point2D(
                newPosition.getX(),
                newPosition.getY()
        ));
    }

    public void setDirection(Point2D newDirection) {
        this.direction = newDirection.normalize();
    }

    public UUID getUUID(){
        return this.uuid;
    }

    public Class<? extends Agent> getAgentType(){
        return this.agentType;
    }

    @Override
    public void display(MovableJfxCamera camera) {
        if (this.sprite == null)
            return;
        this.sprite.display(camera);
    }

    public static Point2D getColliderCenter(Rectangle2D r){
        return new Point2D(
                r.getMinX() + r.getWidth()/2,
                r.getMinY() + r.getHeight()/2
        );
    }

    public static double getMinDistBetweenColliders(Rectangle2D r1, Rectangle2D r2){
        return Math.max(
                Math.abs(getColliderCenter(r1).getX() - getColliderCenter(r2).getX()) - (r1.getWidth() + r2.getWidth()) / 2,
                Math.abs(getColliderCenter(r1).getY() - getColliderCenter(r2).getY()) - (r1.getHeight() + r2.getHeight()) / 2
        );
    }

}
