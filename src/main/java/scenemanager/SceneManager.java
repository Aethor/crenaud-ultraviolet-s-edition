package scenemanager;

import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class SceneManager {
    private List<SceneDatas> sceneDatas;
    private Integer currentSceneIndice;
    private Stage stage;
    private SceneController currentSceneController;

    public SceneManager(String path, Stage stage){

        this.stage = stage;

        Scanner file = new Scanner(getClass().getResourceAsStream(path));
        this.sceneDatas = new LinkedList<SceneDatas>();
        while (file.hasNextLine()){
            String line = file.nextLine();
            this.sceneDatas.add(new SceneDatas(line));
        }
        if (this.sceneDatas.isEmpty()) {
            this.currentSceneIndice = null;
            this.currentSceneController = null;
        }
        else {
            this.currentSceneIndice = 0;
            this.loadCurrentScene();
        }
    }

    public SceneDatas getCurrentSceneDatas(){
        if (this.currentSceneIndice == null)
            return null;
        return this.sceneDatas.get(this.currentSceneIndice);
    }

    public SceneController getCurrentSceneController(){
        return this.currentSceneController;
    }

    public void loadCurrentScene(Object... args){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            URL location = getClass().getClassLoader().getResource(this.sceneDatas.get(this.currentSceneIndice).fxmlPath);
            Parent root = fxmlLoader.load(location.openStream());
            Scene scene = new Scene(root);
            this.stage.setScene(scene);
            this.currentSceneController = fxmlLoader.getController();
            this.currentSceneController.load(this, "", args);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Error loading scene");
        }
        this.stage.show();
    }

    public boolean next(Object... args){
        if (this.sceneDatas.isEmpty()
                || this.currentSceneIndice == null
                || this.isAtLastScene()) {
            return false;
        }
        this.currentSceneIndice += 1;
        this.loadCurrentScene(args);
        return true;
    }

    public boolean previous(){
        if (this.sceneDatas.isEmpty()
                || this.currentSceneIndice == null
                || this.isAtFirstScene()){
            return false;
        }
        this.currentSceneIndice -= 1;
        this.loadCurrentScene();
        return true;
    }

    public boolean isAtFirstScene(){
        return (this.currentSceneIndice != null
                && this.sceneDatas != null
                && this.currentSceneIndice == 0);
    }

    public boolean isAtLastScene(){
        return (this.currentSceneIndice != null
                && this.sceneDatas != null
                && this.currentSceneIndice == this.sceneDatas.size() - 1);
    }

}
