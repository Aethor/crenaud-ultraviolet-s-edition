package scenemanager;

public class SceneDatas {

    public String fxmlPath;

    public SceneDatas(String initializationString){
        String[] splitted = initializationString.split(":");
        this.fxmlPath = splitted[0];
    }
}
