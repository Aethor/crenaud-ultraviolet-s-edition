package scenemanager;

public abstract class SceneController {
    protected SceneManager sceneManager;

    public void load(SceneManager sceneManager, String configFilePath, Object... args){
        this.sceneManager = sceneManager;
    }

    public void stop(){}
}
