% IA51 : Crenaud UltraViolet Final Progress Report
% Angeline Biot, Jean Porée, Romain Lallemand, Arthur Amalvy

![](../March/logo1.png)

\newpage


# Introduction

These last two weeks have been dedicated to the actual agent programming, and to the interface functionnalities.


# Agent perceptions

## Agent body and proxy design pattern

In *Crenaud UltraViolet*, each agent (except RaptorTacos, as he is not a physical agent) has an equivalent of a body, called his *manifestation* (to be more precise, it is called *MexicanManifestation* in the code). When receiving perceptions, the agent can also probe his *manifestation* to get additionnal informations (his position, his center, his current direction, etc...). 

However, the agent is not authorised to modify his body directly ! He has to request the environnement, that checks the validity of the move. To solve this problem, we used the *proxy design pattern*. The agent is never holding a direct reference to his body : rather, he holds a reference to a *proxy* holding a reference to its body. This *proxy* is designed to let the agent access only to the attributes he needs, and to send only copies of them, preventing the agent to modify the actual values contained in his body.

![A simplified view of the Proxy design pattern used](./proxy.png)


## Perceptions

In order to display an intelligent behaviour, an agent first needs to perceive a part of its environnement. In *Crenaud UltraViolet*, this is supported thanks to our *quadtree* implementation. 

The environnement send perceptions to each agent periodically. This perception is defined by the *Perception* event, and contains a list of entities seen by the agent. To compute this list of entities, the environnement makes a request to the *quadtree*, asking which entities are colliding with the perception shape of each agent. 


# Agent behaviours

## Skills and Capacities

In order to define common behaviours for agents, one needs to define Capacities. Two of them have been used throughout this project : the *MoveCapacity* and the *HitCapacity* (implemented, respectively, by the *BasicMoveSkill* and the *BasicHitSkill*).


### Movement Capacity & Skill

The movement capacity is used by agents to move themselves across the space of the city, while avoiding obstacles. It defines 4 functions :

```java
public interface MoveCapacity extends Capacity {
    public Point2D directionToBuilding(
            Building building,
            MexicanManifestationProxy mexicanManifestationProxy,
            ArrayList<Spatialisable> perceivedEntities
    );

    public Point2D directionToTarget(
            Point2D target,
            MexicanManifestationProxy mexicanManifestationProxy,
            ArrayList<Spatialisable> perceivedEntities
    );

    public Point2D groupDirection(
            Class<? extends Mexican> groupType,
            ArrayList<Spatialisable> perceivedEntities,
            Rectangle2D collider
    );

    public Point2D directionToGroup(
            Class<? extends Mexican> groupType,
            ArrayList<Spatialisable> perceivedEntities,
            Point2D center
    );
}
```

Each function returns a specific direction, which can then be used by the agent to compute his next desired direction.


### Hit Capacity & Skill

The hit capacity is used by aggressive agents, allowing them to hit surrounding agents. It defines a single function :

```java
public interface HitCapacity extends Capacity {
    public long hitInRange(
            ArrayList<Spatialisable> perceivedEntities,
            ArrayList<Class> targets,
            Rectangle2D collider,
            int hitFrequency,
            long timeSinceLastHit,
            long timeOffset,
            DefaultContextInteractions defaultContextInteractions,
            UUID environnementId
    );
}
```

This function is designed to make an agent able to hit surrounding targets agents or buildings at a target frequency. Hitting a target is done by sending a *HitEvent* or a *BuildingHitEvent* to the environnement, asking it to decrease the hitpoints of the targeted agent. If those hitpoints fall below 0, the agent dies.

## Agents types

### Civilians

*Civilians* are the poor victims of the *UltraViolet riot*. They just wants to live their day to day life.

When created, a *Civilian* is given two different buildings, and will travel between them indefinitely. 


### UltraViolets

*UltraViolets* are violent rioters, ready to destroy everything. However, despite their violence, they are still somewhat coordinated.

Firstly, they use social networks. Each UltraViolet can randomly check the *SombreraGram* website on his smartphone, seeing the last RaptorTacos post. This post can contain a building as a target : if seen, the UltraViolet will use its *BasicMoveSkill* to move towards the building in order to destroy it.

Secondly, UltraViolets tends to group. Using his BasicMoveSkill *groupDirection* function, an UltraViolet will try to mimic the direction of other close UltraViolets rioters.

As violent agents, UltraViolets also possess an instance of the *BasicHitSkill*, and will try to hit every non-neutral building or mercenary in their surroundings.


### Mercenaries

*Mercenaries* are the defenders of the city, and are spawned by the environnement periodically in buildings.

When no UltraViolet are near, mercenaries will patroll in a randomly generated rectangle around their spawn points, using their *BasicMoveSkill*.

However, when seeing an UltraViolet, a Mercenary will charge it to attack him. When the UltraViolet is dead or out of reach, the Mercenary will return to it's original patroll route.


### The troubled case of @RaptorTacos

While Raptor Tacos is not a physical agent, he still has an influence on the game. He randomly sends *SombreraPosts*, some of them containing a building, acting as a target for UltraViolets rioters.