% IA51 : Crenaud UltraViolet Second Progress Report
% Angeline Biot, Jean Porée, Romain Lallemand, Arthur Amalvy

![](../March/logo1.png)

\newpage


# Introduction

This month, we started to work on the environnement, on the global behaviour of the agent system and on the user interface.

*Note : For reasons we will explain below, the project has been migrated to https://gitlab.com/Aethor/crenaud-ultraviolet-s-edition*


# The Scene Manager module

As the game needed a module to manage easily JavaFX scenes, we created a scene manager module. This module has multiple responsabilities :

* Load all representations of scenes at the start of the application by reading a specification file
* Load the first scene as a JavaFX scene at the start of the application
* Propose an API for JavaFX scene controllers to manage transitions between scenes
* Send settings to a JavaFX controller at its creation
* Unload everything in the current scene when the application ends

![The scene manager module](./scene_manager.png)

# Environnement

## General structure

![Simplified overview of the agent system](./environnement_general.png)

The environnement periodically send events to each mexican in the system, by sending a *Perception* event. This perception event contains every object found in a rectangle in front of a mexican, computed by the *Quadtree*. The mexican is then free to react, and can do multiples things : amongst them are the *move* and the *hit* action.

The *hit* action does not require any update : the mexican will only send a hit event to the mexican he wants to hit.

The *move* action is different : the environnement needs to know when agent moves, in order to generate perception and to draw the game. The mexican will then fire a *PositionUpdate* event, that the environnement will use (if the move is considered valid) to update it's internal representation of the mexican position (in his *MexicanManifestation*, that can be seen as the agent body).

## Events

The following events are already defined, but more of them may be defined later :

* Perception
    * Collection<Building> : List of buildings seen by the current mexican
    * Collection<Mexican> : List of other mexicans seen by the current mexican
* Hit
    * int : Damage inflicted to the current mexican
* PositionUpdate
    * Point2D : the translation vector representing the movement


# Migration to Java

Earlier this month, we decided to move to Java for the implementation of the project, but to still use the SARL API and the Janus runtime environnement. While we still believe SARL as a language is pretty capable when it comes to agent oriented programming, we think this switch to Java will allow us to work faster and as such, to deliver a better finished game, for a number of reasons.

The first of these reasons is the tooling support : the whole team is familiar with the IntelliJ platform, that can't be used in a vanilla SARL install. The move to IntelliJ also comes with better multiplatform JavaFX integration, as IntelliJ can supply it's own JavaFX runtime.

The only downside of the switch is the added complexity of using the SARL API from Java : however, this is mitigated by our prior knowledge of Java. 