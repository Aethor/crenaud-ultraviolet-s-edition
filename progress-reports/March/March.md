% IA51 : Crenaud UltraViolet First Progress Report
% Angeline Biot, Jean Porée, Romain Lallemand, Arthur Amalvy

![](./logo1.png)

\newpage

# Introduction

Have you ever wondered about how the universe was strange ? Well, you were right. In fact, only a few of these weird things the universe is capable of ever reach our hears. It happens to exist an alternate reality, in parallel of our world. This reality, due to the the curious randomness of laws we don't understand, is strictly identical to our own, if we forget about a tiny little detail : in this alternate universe, Nazis won World War Two. But it's not the story we are gonna tell you today.

After the failure of **Donald Trump** to contain mexican immigration at the U.S. border using his great wall, the mexicans took control of the entire world. With the guidance of the new mexican supreme leader, **Jimmy Neutral**, the *Great Mexican Empire* seems bound to last a thousand years.

However, a last hurdle remains : the *UltraViolet* cult. With the help of the *SombreraGram* social platform, the *UltraViolet* cult gathered a critical mass of followers, and is now threatening to overthrow the young *Mexican Empire*. Using the fact that all governement members are colorblind and can't distinguish violet, the cult equipped all its followers with violet clothes in order to render them invisible, and is now starting an all-out riot. It's up to you to save the *Great Mexican Empire*...


# First definition

The first seance was dedicated to the project definition. It has been decided to create a new game in the Crenaud series : Crenaud UltraViolet. 

This game will be centered around riot simulation. The main goal of the player will be to defend its city against rioters. To do so, he can create different buildings type.

## The City

The city is comprised of four types of buildings :

* The *Presidential Palace*. If this building is destroyed by rioters, the player loses.
* *Tacos* restaurants. A *Tacos* increases the amount of money passively gained by the player.
* Mercenary *Camps*. A *Camp* will periodically spawn mercenaries, the main defense unit of the player
* *Neutral Buildings*.


## Mexicans

Most actors of the game are *Mexicans*. Each *Mexican* is an agent, and four different types of *Mexican* exist :

* *UltraViolets* are rioters, and want to take down the player.
* *Mercenaries* are the city defenders.
* *Civilians* are neutral *Mexicans*.
* *The President* is the most important unit : if he dies, the player loses. The *President* usually lies in the *Presidential Palace*, but he can choose to leave it to perform a *Presidential Visit*.


## Sombreragram

![An early version of the Sombreragram logo](./sombreragram.png)

Sombreragram is a social media, used by *UltraViolets* to coordinate their actions. The player has full access to Sombreragram, and therefore can anticipates rioters moves. Periodically, *RaptorTacos*, an influent member of the rioters cult, posts messages to *Sombreragram*. There are two types of sombreragram post :

* *Neutral Posts* are only intended to be read by the player, to develop the background story of the game
* *Targeted Posts* targets a specific building. Rioters reading this post will attempt to gather at the location of the targeted building to destroy it.


# Player interactions

Possible players action are as follows :

* Transform a *Neutral Building* into a *Camp*, by paying a defined amount of money. A *Camp* will periodically spawn mercenaries, the main defense unit of the player
* Transform a *Neutral Building* into a *Tacos*, by paying a defined amount of money. A *Tacos* will increase the amount of money the player earns passively

More actions may be added, if the game does not feel satisfactory to play (for example, the ability to destroy building to regain a subset of their price).


# Ontology

![Ontology of the game](./ontology.png)

\newpage

# Use cases

![General use case](./use_case_1.png)

![Building creation use case](./use_case_2.png)

