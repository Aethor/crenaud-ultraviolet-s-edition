% IA51 : Crenaud UltraViolet Second Progress Report
% Angeline Biot, Jean Porée, Romain Lallemand, Arthur Amalvy

![](../March/logo1.png)

\newpage

# Introduction

You can now find the project on gitlab : https://gitlab.com/the-crenaud-project/crenaud-ultraviolet

This month, we focused on :

* The global structure of the project, by defining more closely some interactions and relations, for example with sequence diagrams
* The design & implementation of two submodules : the Camera and the Quadtree modules
* Some "quality of life" problems, mainly dependency resolution using maven


# Project Structure


## Organisation diagrams 

![Organisation diagram representing the interaction of agents with the environnement](./economy_organisation.jpg)

![Organisation diagram representing interactions between all types of mexican agents](./mexican_organisation.jpg)

## Sequence diagrams

![Sequence diagram of interactions between mexican agents and the environnement](./mexican_sequence.jpg)

![Sequence diagram of interactions between the user and the environnement](./user_sequence.jpg)

![Sequence diagram describing agent spawning by the environnement](./env_sequence_creation.jpg)

![Sequence diagram describing behaviours concerning the special "Jimmy Neutral" agent](./jimmy_neutral_sequence.jpg)

# Optimising interactions : a Quadtree module

Crenaud UltraViolet is a game where a lot of agents will be interacting. Therefore, a specialised datastructure needs to be implemented in order to deliver correct performances. 

We spent some time searching for the most optimised data-structure for our use-cases. We investigated R-trees, kd-trees, etc... and finally set our mind to quadtrees. They are efficient enough for our use cases (collision detection, spatial requests...), without being too hard to implement.

A Quadtree is a kind of datastructure making use of the classic "divide and conquer" strategy. At the simplest level, it is simply a tree where each node have 0 or 4 children. Each leaf node contains 0 or more "things" it is supposed to store. 

In our case, the Quadtree is a spatial Quadtree. Each of its node represents a portion of the space. If the node is at the top level of the tree, it represents the whole space of the tree. Otherwise, it represents a quarter of the space represented by its parent node.

You can find our implementation of a spatial Quadtree, named Quadflow, here : https://gitlab.com/Aethor/quadflow

A demo is packaged with the library to test the quadtree capacity.

![Quadflow in action](./quadtree.png)


# Drawing on the screen : the Camera module

https://gitlab.com/Naej/simple2drendering

We created a library called Simple2DRendering, it's a library providing an implementation of a javaFX 2D rendering camera and an interface called Displayable describing the basic function you have to implement to create new displayable objects.
The camera is used to transform coordinates from the simulation space to the display space and holds the objects used to render the scene (a JavaFX canvas).
To display obects with the camera you have to call the display function and give it an Iterable of Displayable.
The Displayable interface contains only one function: the display function, which takes the current camera as argument, so it can use the camera functions to transform its coordinates and display itself.

![UML diagram of the camera module](./camera_uml.jpg)

# Project gestion : Maven and Jitpack

The last two modules we presented here are independant dependencies, living on their own git repositories. Therefore, we needed a way to include them in the project.

Using Maven, we can easily include dependencies to the project. However, the other two projects first need to be published on a Maven repository, but publishing on such a repository is long and cumbersome.

Hopefully, a solution exists : [Jitpack.io](https://jitpack.io). This service can fetch a git repository, and create a virtual maven repository containing your git repository, allowing you to specify a git repository as a dependency.

Example pom.xml :

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<groupId>net.crenaud</groupId>
	<artifactId>crenaud-uv</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	
	<repositories>
		<repository>
			<id>jitpack.io</id>
			<url>https://jitpack.io</url>
		</repository>
	</repositories>

	<properties>
		<sarl.version>0.9.0</sarl.version>
		<compiler.level>1.8</compiler.level>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>
		<dependency>
			<groupId>io.sarl.maven</groupId>
			<artifactId>io.sarl.maven.sdk</artifactId>
			<version>${sarl.version}</version>
		</dependency>
		<dependency>
			<groupId>com.gitlab.Aethor</groupId>
			<artifactId>quadflow</artifactId>
			<version>-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>com.gitlab.Naej</groupId>
			<artifactId>simple2drendering</artifactId>
			<version>-SNAPSHOT</version>
		</dependency>
	</dependencies>

	...

</project>
```
